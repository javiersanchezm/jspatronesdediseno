/*
Usando el patrón observador

Mensajes tipo Meet
Mini apicación de chat local (de nostros a nosotros), una pantalla donde se van a escribir los mensajes
abajo una caja de texto donde se va a escribir un mensaje, y un botón enviar mensaje
Se tiene que mostrar un texto, clase o indicador de que les llego un mensaje le llega la
notificación a todos menos a la persona que envio el mensaje.
abajo se tendrán los usuarios, se debe seleccionar uno para enviar el mensaje, y notificar a los
demás con una esfera que les llego un mensaje ya sea que se ponga en negrita o agregar icono
Cuadro, input y botón enviar
*/

const $listaComentarios = document.getElementById("comentarios");
const $comentario = document.getElementById("comentario");
const $enviar = document.getElementById("enviar");
const $usuario = document.getElementById("usuario");
const $notificacion1 = document.getElementsByClassName("usr");

let usuarioSeleccionado = 0;
let usuario = "";

class Observador {
    constructor(){
        this.subscriptores = [];
    }

    subscribir(subscriptor){
        this.subscriptores.push(subscriptor);
    }

    notificar(evento){
        $comentario.value = "";
        this.subscriptores.map((subscriptor,index) => {
            usuario = usuarioSeleccionado;
            $notificacion1[index].style.border = "";
            $notificacion1[index].style.boxShadow = "";
            if(subscriptor.nombre != usuario){
                subscriptor.mensaje.call(subscriptor, evento);
                $notificacion1[index].style.border = "4px dotted blue";
                $notificacion1[index].style.boxShadow = "0px 0px 10px blue";
            }
        });
    }
}

class Subscriptor{

    constructor(nombre){
        this.nombre = nombre;
    }

    mensaje(evento){
        console.log(`Notificar ${this.nombre} que han comentado: ${evento}`);
    }
}


const observador = new Observador();
const usr1 = new Subscriptor("Pablo");
const usr2 = new Subscriptor("Pedro");
const usr3 = new Subscriptor("Maria");
const usr4 = new Subscriptor("Jorge");


$usuario.addEventListener( "click", (evento) => {
    console.log(evento.target.textContent);
    usuarioSeleccionado = evento.target.textContent;
});


$enviar.addEventListener( "click", ()=> {
    
    if($comentario.value != "" && usuarioSeleccionado != ""){

        const $div = document.createElement("div");
        const $p = document.createElement("p");
        const $titulo = document.createElement("h1");
        const $hora = document.createElement("span");

        let hora = new Date();
        //$imagen.src = $notificacion1[0].src;
        $titulo.textContent = usuarioSeleccionado;
        $hora.textContent = `${hora.getHours()} : ${hora.getMinutes()} : ${hora.getSeconds()}`;
        $p.textContent = $comentario.value;

        $div.appendChild($titulo);
        $titulo.appendChild($hora);
        $div.appendChild($p);
        $listaComentarios.appendChild($div);

        observador.subscribir(usr1);
        observador.subscribir(usr2);
        observador.subscribir(usr3);
        observador.subscribir(usr4);

        observador.notificar("USUARIO QUE COMENTO FUE: "+ usuarioSeleccionado);
        usuarioSeleccionado = "";
    }
});
